# offboard_customer

### To be used as part of the offboarding workflow for customers ###
### This cookbook removes crusers from a customer instance and removes the sudoer file. ###
* $ knife node run_list remove <node_name> recipe[cr_sshkey_deploy]
* $ knife node run_list add <node_name> recipe[offboard_customer]
* $ knife node delete testnode -y
* $ knife client delete testnode -y