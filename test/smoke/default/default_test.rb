# # encoding: utf-8

# Inspec test for recipe offboard_customer::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

crusers = [ 
# knife data bag show crusers -F json
  'cr.alberto.alvarez',
  'cr.finlay.wright',
  'cr.neil.stewart',
  'cr.sebastian.kasprzak',
  'cr.allen.millora',
  'cr.gordon.thomson',
  'cr.nishan.vivekanandan',
  'cr.shannon.murray',
  'cr.antoine.noel',
  'cr.jared.chia',
  'cr.paul.jay',
  'cr.shanoor.hussain',
  'cr.asser.hassan',
  'cr.paul.stockinger',
  'cr.shaw.taheri',
  'cr.ben.ezard',
  'cr.kieran.doonan',
  'cr.shona.mckenzie',
  'cr.liam.caproni',
  'cr.cloud.insight',
  'cr.david.elliott',
  'cr.marco.ballerini',
  'cr.rashid.niazi',
  'cr.sam.humphrey',
  'cr.tamas.millian',
  'cr.teresa.to',
]

crusers.each do |cruser|
  describe user(cruser) do
    it { should_not exist }
  end
  describe directory("/home/#{cruser}") do
    it { should_not exist }
  end
end

describe file('/etc/sudoers.d/crusers') do
  it { should_not exist }
end
