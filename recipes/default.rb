#
# Cookbook:: offboard_customer
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

unless node['cr_ssh_deploy']['config_bags'].empty?
  node['cr_ssh_deploy']['deployed_users'].each do |user_id|
    user user_id do
      action :remove
      force true
    end
    directory "/home/#{user_id}" do
      only_if { File.directory? "/home/#{user_id}" }
      recursive true
      action :delete
    end
  end
end

file '/etc/sudoers.d/crusers' do
  only_if { File.exist? '/etc/sudoers.d/crusers' }
  action :delete
end
